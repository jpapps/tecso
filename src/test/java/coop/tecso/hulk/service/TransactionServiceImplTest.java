package coop.tecso.hulk.service;

import coop.tecso.hulk.api.dto.*;
import coop.tecso.hulk.api.service.ProductService;
import coop.tecso.hulk.api.service.TransactionService;
import coop.tecso.hulk.entity.Transaction;
import coop.tecso.hulk.entity.TransactionProduct;
import coop.tecso.hulk.repository.TransactionProductRepository;
import coop.tecso.hulk.repository.TransactionRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

public class TransactionServiceImplTest {

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    private TransactionProductRepository transactionProductRepository;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private ModelMapper modelMapperTransactionProduct;

    @Mock
    private ProductService productService;

    @InjectMocks
    private TransactionService transactionService = new TransactionServiceImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateTransactionExpectedNotInternalServer() {
        TransactionDTO transactionDTO = new TransactionDTO();

        ResponseDTO responseDTO = transactionService.save(transactionDTO);

        Assert.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseDTO.getHttpStatus());
    }

    @Test
    public void testCreateTransactionExpectedNotAceptable() {
        List<TransactionProductDTO> transactionProductDTOList = new ArrayList<>();

        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setTransactionProductDTOList(transactionProductDTOList);

        ResponseDTO responseDTO = transactionService.save(transactionDTO);

        Assert.assertEquals(HttpStatus.NOT_ACCEPTABLE, responseDTO.getHttpStatus());
    }

    @Test
    public void testCreateTransactionExpectedOK() {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setId(1L);

        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(1L);

        TransactionDTO transactionDTO = new TransactionDTO();
        transactionDTO.setCustomerDTO(customerDTO);
        transactionDTO.setEmployeeDTO(employeeDTO);

        List<TransactionProductDTO> transactionProductDTOList = new ArrayList<>();

        TransactionProductDTO transactionProductDTO = new TransactionProductDTO();
        transactionProductDTO.setProductDTO(new ProductDTO());
        transactionProductDTO.setTransactionDTO(transactionDTO);
        transactionProductDTOList.add(transactionProductDTO);
        transactionDTO.setTransactionProductDTOList(transactionProductDTOList);

        when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(new Transaction());
        when(transactionRepository.save(Mockito.any())).thenReturn(new Transaction());
        when(transactionProductRepository.save(Mockito.any())).thenReturn(new TransactionProduct());
        when(productService.getById(Mockito.any())).thenReturn(new ResponseDTO());
        when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(new TransactionProduct());
        when(productService.save(Mockito.any()));

        ResponseDTO responseDTO = transactionService.save(transactionDTO);

        Assert.assertEquals(HttpStatus.OK, responseDTO.getHttpStatus());
    }

}

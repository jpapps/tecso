package coop.tecso.hulk.service;

import coop.tecso.hulk.api.dto.ProductDTO;
import coop.tecso.hulk.api.service.ProductService;
import coop.tecso.hulk.entity.Product;
import coop.tecso.hulk.repository.ProductRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class ProductServiceImplTest {

    @Rule
    public final ExpectedException expected = ExpectedException.none();

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private ProductService productService = new ProductServiceImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateProduct() {
        Product product = new Product();
        product.setId(1L);

        ProductDTO productDTO = new ProductDTO();
        productDTO.setQuantity(20);
        productDTO.setCode("PRO001");
        productDTO.setDescription("Camisa Hulk");
        productDTO.setPrice(26.0);

        when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(product);
        when(productRepository.save(product)).thenReturn(product);

        Long id = ((Product) productService.save(new ProductDTO()).getObject()).getId();

        Assert.assertEquals(id, product.getId());

    }

    @Test
    public void testCreateProductExpetedInternalServerError() {

        HttpStatus status = productService.save(null).getHttpStatus();

        Assert.assertEquals(status, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @Test
    public void testUpdateProduct() {
        Product product = new Product();
        product.setId(1L);

        ProductDTO productDTO = new ProductDTO();
        productDTO.setQuantity(20);
        productDTO.setCode("PRO001");
        productDTO.setDescription("Camisa Hulk");
        productDTO.setPrice(26.0);

        when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(product);
        when(productRepository.save(product)).thenReturn(product);

        Long id = ((Product) productService.update(new ProductDTO()).getObject()).getId();

        Assert.assertEquals(id, product.getId());

    }

    @Test
    public void testUpdateProductExpetedInternalServerError() {

        HttpStatus status = productService.update(null).getHttpStatus();

        Assert.assertEquals(status, HttpStatus.INTERNAL_SERVER_ERROR);

    }

    @Test
    public void testDeleteProductExpected200OK() {
        Product product = new Product();
        product.setId(1L);

        doNothing().when(productRepository).deleteById(Mockito.any());

        HttpStatus status = productService.delete(product.getId()).getHttpStatus();

        Assert.assertEquals(status, HttpStatus.OK);
    }

}

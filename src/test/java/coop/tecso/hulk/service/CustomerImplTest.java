package coop.tecso.hulk.service;


import coop.tecso.hulk.api.dto.CustomerDTO;
import coop.tecso.hulk.api.dto.ResponseDTO;
import coop.tecso.hulk.api.service.CustomerService;
import coop.tecso.hulk.entity.Customer;
import coop.tecso.hulk.repository.CustomerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;

import static org.mockito.Mockito.when;

public class CustomerImplTest {

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private CustomerService customerService = new CustomerServiceImpl();

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateCustomerExpetedOK() {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(1L);
        customerDTO.setAddress("prueba");
        customerDTO.setAge("19");
        customerDTO.setMail("prueba@gmail.com");
        customerDTO.setName("prueba prueba");

        when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(new Customer());
        when(customerRepository.save(Mockito.any())).thenReturn(new Customer());

        ResponseDTO responseDTO = customerService.save(customerDTO);

        Assert.assertEquals(responseDTO.getHttpStatus(), HttpStatus.CREATED);
    }

    @Test
    public void testCreateCustomerExpetedInternalServerError() {

        ResponseDTO responseDTO = customerService.save(null);

        Assert.assertEquals(responseDTO.getHttpStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testListAllCustomerExpetedInternalServerError() {

        ResponseDTO responseDTO = customerService.listAll(null,null);

        Assert.assertEquals(responseDTO.getHttpStatus(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}

package coop.tecso.hulk.rest;

import coop.tecso.hulk.api.dto.ResponseDTO;
import coop.tecso.hulk.api.dto.TransactionDTO;
import coop.tecso.hulk.api.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/transaction")
public class TransactionController {

    @Autowired
    private TransactionService transactionService;

    @PostMapping
    public ResponseDTO create(@RequestBody TransactionDTO transactionDTO) {
        return transactionService.save(transactionDTO);
    }

    @GetMapping
    public ResponseDTO getAll(@RequestParam(name = "page") Integer page, @RequestParam(name = "size") Integer size) {
        return transactionService.listAll(page, size);
    }

}

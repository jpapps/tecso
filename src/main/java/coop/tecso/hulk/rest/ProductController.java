package coop.tecso.hulk.rest;

import coop.tecso.hulk.api.dto.ProductDTO;
import coop.tecso.hulk.api.dto.ResponseDTO;
import coop.tecso.hulk.api.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping
    public ResponseDTO create(@RequestBody ProductDTO productDTO) {
        return productService.save(productDTO);
    }

    @PutMapping
    public ResponseDTO update(@RequestBody ProductDTO productDTO) {
        return productService.update(productDTO);
    }

    @DeleteMapping
    public ResponseDTO delete(@RequestParam(name = "id") Long id) {
        return productService.delete(id);
    }

    @GetMapping
    public ResponseDTO getAll(@RequestParam(name = "page") Integer page, @RequestParam(name = "size") Integer size) {
        return productService.listAll(page, size);
    }
}

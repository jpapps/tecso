package coop.tecso.hulk.rest;

import coop.tecso.hulk.api.dto.ResponseDTO;
import coop.tecso.hulk.api.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public ResponseDTO getEmployee(@RequestParam(name = "page") Integer page, @RequestParam(name = "size") Integer size) {
        return employeeService.getEmployes(page, size);
    }

}

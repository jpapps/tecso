package coop.tecso.hulk.rest;

import coop.tecso.hulk.api.dto.CustomerDTO;
import coop.tecso.hulk.api.dto.ResponseDTO;
import coop.tecso.hulk.api.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping
    public ResponseDTO create(@RequestBody CustomerDTO customerDTO) {
        return customerService.save(customerDTO);
    }

    @GetMapping
    public ResponseDTO getAll(@RequestParam(name = "page") Integer page, @RequestParam(name = "size") Integer size) {
        return customerService.listAll(page, size);
    }
}

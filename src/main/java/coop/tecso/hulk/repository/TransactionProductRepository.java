package coop.tecso.hulk.repository;

import coop.tecso.hulk.entity.TransactionProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransactionProductRepository extends JpaRepository<TransactionProduct, Long> {
}

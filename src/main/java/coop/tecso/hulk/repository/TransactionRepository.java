package coop.tecso.hulk.repository;

import coop.tecso.hulk.entity.Transaction;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Long> {
}

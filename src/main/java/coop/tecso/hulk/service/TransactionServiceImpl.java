package coop.tecso.hulk.service;

import coop.tecso.hulk.api.dto.ProductDTO;
import coop.tecso.hulk.api.dto.ResponseDTO;
import coop.tecso.hulk.api.dto.TransactionDTO;
import coop.tecso.hulk.api.dto.TransactionProductDTO;
import coop.tecso.hulk.api.enums.APITransactionCodeEnum;
import coop.tecso.hulk.api.enums.StatusTransactionEnum;
import coop.tecso.hulk.api.service.ProductService;
import coop.tecso.hulk.api.service.TransactionService;
import coop.tecso.hulk.entity.Transaction;
import coop.tecso.hulk.entity.TransactionProduct;
import coop.tecso.hulk.repository.TransactionProductRepository;
import coop.tecso.hulk.repository.TransactionRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class TransactionServiceImpl implements TransactionService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);

    private static final String ERROR = "An error has occurred";

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private TransactionProductRepository transactionProductRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ResponseDTO save(TransactionDTO transactionDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            responseDTO = isValid(transactionDTO, responseDTO);
            if (responseDTO.getHttpStatus().equals(HttpStatus.NOT_ACCEPTABLE)) {
                return responseDTO;
            }
            transactionDTO.setStatus(StatusTransactionEnum.IN_PROGRESS);
            transactionDTO.setCreatedAt(Calendar.getInstance().getTime());
            transactionDTO.setId(transactionRepository.save(modelMapper.map(transactionDTO, Transaction.class)).getId());

            for (TransactionProductDTO transactionProductDTO : transactionDTO.getTransactionProductDTOList()) {
                ProductDTO productDTO = (ProductDTO) productService.getById(transactionProductDTO.getProductDTO().getId()).getObject();
                if (productDTO != null && productDTO.getQuantity() == 0) {
                    throw new Exception();
                }
                transactionProductDTO.setTransactionDTO(transactionDTO);
                transactionProductDTO.setProductDTO(productDTO);
                transactionProductRepository.save(modelMapper.map(transactionProductDTO, TransactionProduct.class));
                productDTO.setQuantity(productDTO.getQuantity() - 1);
                productService.update(productDTO);
            }

            responseDTO.setMessage(APITransactionCodeEnum.TRA001.getDescription());
            responseDTO.setHttpStatus(HttpStatus.CREATED);
        } catch (Exception e) {
            responseDTO.setMessage(APITransactionCodeEnum.TRA002.getDescription());
            responseDTO.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            responseDTO.setException(e.getMessage());
            logger.error(ERROR, e);
        }
        return responseDTO;
    }

    private ResponseDTO isValid(TransactionDTO transactionDTO, ResponseDTO responseDTO) {
        if (transactionDTO.getTransactionProductDTOList().isEmpty()) {
            responseDTO.setObject(transactionDTO);
            responseDTO.setHttpStatus(HttpStatus.NOT_ACCEPTABLE);
            responseDTO.setMessage(APITransactionCodeEnum.TRA003.getDescription());
            return responseDTO;
        }
        if (transactionDTO.getEmployeeDTO() == null || transactionDTO.getEmployeeDTO().getId() == 0) {
            responseDTO.setObject(transactionDTO);
            responseDTO.setHttpStatus(HttpStatus.NOT_ACCEPTABLE);
            responseDTO.setMessage(APITransactionCodeEnum.TRA004.getDescription());
            return responseDTO;
        }
        if (transactionDTO.getCustomerDTO() == null && transactionDTO.getCustomerDTO().getId() == 0) {
            responseDTO.setObject(transactionDTO);
            responseDTO.setHttpStatus(HttpStatus.NOT_ACCEPTABLE);
            responseDTO.setMessage(APITransactionCodeEnum.TRA005.getDescription());
            return responseDTO;
        }
        responseDTO.setHttpStatus(HttpStatus.ACCEPTED);
        return responseDTO;
    }

    @Override
    public ResponseDTO listAll(Integer page, Integer size) {
        ResponseDTO responseDTO = new ResponseDTO();
        Pageable sortedByName =
                PageRequest.of(page, size, Sort.by("createdAt"));
        try {
            responseDTO.setObjectPage(transactionRepository.findAll(sortedByName).map(this::convertToDto));
            responseDTO.setMessage(APITransactionCodeEnum.TRA007.getDescription());
            responseDTO.setHttpStatus(HttpStatus.FOUND);
        } catch (Exception e) {
            responseDTO.setException(e.getMessage());
            responseDTO.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            responseDTO.setMessage(APITransactionCodeEnum.TRA008.getDescription());
            logger.error(ERROR, e);
        }
        return responseDTO;
    }

    private TransactionDTO convertToDto(Transaction transaction) {
        return modelMapper.map(transaction, TransactionDTO.class);
    }
}

package coop.tecso.hulk.service;

import coop.tecso.hulk.api.dto.CustomerDTO;
import coop.tecso.hulk.api.dto.ResponseDTO;
import coop.tecso.hulk.api.enums.APICustomerCodeEnum;
import coop.tecso.hulk.api.service.CustomerService;
import coop.tecso.hulk.entity.Customer;
import coop.tecso.hulk.repository.CustomerRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class CustomerServiceImpl implements CustomerService {

    private static final Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private static final String ERROR = "An error has occurred";

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ResponseDTO save(CustomerDTO customerDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            customerDTO.setCreateAt(Calendar.getInstance().getTime());
            responseDTO.setObject(customerRepository.save(modelMapper.map(customerDTO, Customer.class)));
            responseDTO.setMessage(APICustomerCodeEnum.CUS001.getDescription());
            responseDTO.setHttpStatus(HttpStatus.CREATED);
        } catch (Exception e) {
            responseDTO.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            responseDTO.setMessage(APICustomerCodeEnum.CUS002.getDescription());
            responseDTO.setException(e.getMessage());
            logger.error(ERROR, e);
        }
        return responseDTO;
    }

    @Override
    public ResponseDTO listAll(Integer page, Integer size) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            Pageable sortedByName =
                    PageRequest.of(page, size, Sort.by("name"));
            responseDTO.setObjectPage(customerRepository.findAll(sortedByName).map(this::convertToDto));
            responseDTO.setMessage(APICustomerCodeEnum.CUS004.getDescription());
            responseDTO.setHttpStatus(HttpStatus.FOUND);
        } catch (Exception e) {
            responseDTO.setMessage(APICustomerCodeEnum.CUS003.getDescription());
            responseDTO.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            responseDTO.setException(e.getMessage());
            logger.error(ERROR, e);
        }
        return responseDTO;
    }

    private CustomerDTO convertToDto(Customer customer) {
        return modelMapper.map(customer, CustomerDTO.class);
    }
}

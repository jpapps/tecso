package coop.tecso.hulk.service;

import coop.tecso.hulk.api.dto.EmployeeDTO;
import coop.tecso.hulk.api.dto.ResponseDTO;
import coop.tecso.hulk.api.enums.APIEmployeeCodeEnum;
import coop.tecso.hulk.api.service.EmployeeService;
import coop.tecso.hulk.entity.Employee;
import coop.tecso.hulk.repository.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    private static final String ERROR = "An error has occurred";

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ResponseDTO getEmployes(Integer page, Integer size) {
        ResponseDTO responseDTO = new ResponseDTO();
        Pageable sortedByName =
                PageRequest.of(page, size, Sort.by("name"));
        try {
            responseDTO.setObjectPage(employeeRepository.findAll(sortedByName).map(this::convertToDto));
            responseDTO.setMessage(APIEmployeeCodeEnum.EMP001.getDescription());
            responseDTO.setHttpStatus(HttpStatus.FOUND);
        } catch (Exception e) {
            responseDTO.setHttpStatus(HttpStatus.NOT_FOUND);
            responseDTO.setException(e.getMessage());
            responseDTO.setMessage(APIEmployeeCodeEnum.EMP002.getDescription());
            logger.error(ERROR, e);
        }
        return responseDTO;
    }

    private EmployeeDTO convertToDto(Employee employee) {
        return modelMapper.map(employee, EmployeeDTO.class);
    }
}

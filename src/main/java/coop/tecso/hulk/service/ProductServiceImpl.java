package coop.tecso.hulk.service;

import coop.tecso.hulk.api.dto.ProductDTO;
import coop.tecso.hulk.api.dto.ResponseDTO;
import coop.tecso.hulk.api.enums.APIProductCodeEnum;
import coop.tecso.hulk.api.service.ProductService;
import coop.tecso.hulk.entity.Product;
import coop.tecso.hulk.repository.ProductRepository;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.concurrent.locks.ReentrantLock;

@Service
public class ProductServiceImpl implements ProductService {

    ReentrantLock lock = new ReentrantLock();

    private static final Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

    private static final String ERROR = "An error has occurred";

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ResponseDTO save(ProductDTO productDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            productDTO.setCreateAt(Calendar.getInstance().getTime());
            responseDTO.setObject(productRepository.save(modelMapper.map(productDTO, Product.class)));
            responseDTO.setMessage(APIProductCodeEnum.PRD001.getDescription());
            responseDTO.setHttpStatus(HttpStatus.CREATED);
        } catch (Exception e) {
            responseDTO.setMessage(APIProductCodeEnum.PRD002.getDescription());
            responseDTO.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            responseDTO.setException(e.getMessage());
            logger.error(ERROR, e);
        }
        return responseDTO;
    }

    @Override
    public ResponseDTO update(ProductDTO productDTO) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            lock.lock();
            productDTO.setUpdateAt(Calendar.getInstance().getTime());
            responseDTO.setObject(productRepository.save(modelMapper.map(productDTO, Product.class)));
            responseDTO.setMessage(APIProductCodeEnum.PRD004.getDescription());
            responseDTO.setHttpStatus(HttpStatus.OK);
        } catch (Exception e) {
            responseDTO.setMessage(APIProductCodeEnum.PRD005.getDescription());
            responseDTO.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            responseDTO.setException(e.getMessage());
            logger.error(ERROR, e);
        } finally {
            lock.unlock();
        }
        return responseDTO;
    }

    @Override
    public ResponseDTO delete(Long id) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            productRepository.deleteById(id);
            responseDTO.setHttpStatus(HttpStatus.OK);
            responseDTO.setMessage(APIProductCodeEnum.PRD007.getDescription());
        } catch (Exception e) {
            responseDTO.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            responseDTO.setMessage(APIProductCodeEnum.PRD006.getDescription());
            responseDTO.setException(e.getMessage());
            logger.error(ERROR, e);
        }
        return responseDTO;
    }

    @Override
    public ResponseDTO getById(Long id) {
        ResponseDTO responseDTO = new ResponseDTO();
        try {
            responseDTO.setObject(convertToDto(productRepository.findById(id).orElse(new Product())));
            responseDTO.setHttpStatus(HttpStatus.OK);
            responseDTO.setMessage(APIProductCodeEnum.PRD007.getDescription());
        } catch (Exception e) {
            responseDTO.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            responseDTO.setMessage(APIProductCodeEnum.PRD006.getDescription());
            responseDTO.setException(e.getMessage());
            logger.error(ERROR, e);
        }
        return responseDTO;
    }

    @Override
    public ResponseDTO listAll(Integer page, Integer size) {
        ResponseDTO responseDTO = new ResponseDTO();
        Pageable sortedByName =
                PageRequest.of(page, size, Sort.by("description"));
        try {
            responseDTO.setObjectPage(productRepository.findAll(sortedByName).map(this::convertToDto));
            responseDTO.setMessage(APIProductCodeEnum.PRD003.getDescription());
            responseDTO.setHttpStatus(HttpStatus.FOUND);
        } catch (Exception e) {
            responseDTO.setMessage(APIProductCodeEnum.PRD008.getDescription());
            responseDTO.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            responseDTO.setException(e.getMessage());
            logger.error(ERROR, e);
        }
        return responseDTO;
    }

    public ProductDTO convertToDto(Product product) {
        return modelMapper.map(product, ProductDTO.class);
    }
}

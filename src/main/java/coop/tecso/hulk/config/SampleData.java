package coop.tecso.hulk.config;

import coop.tecso.hulk.entity.Employee;
import coop.tecso.hulk.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Calendar;

@Configuration
public class SampleData {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Bean
    public void sampleDate() {
        employeeRepository.save(getEmployee());
    }

    private Employee getEmployee() {
        Employee employee = new Employee();
        employee.setName("DANIEL PRUEBA TORO RINCON");
        employee.setAddress("CR 48 A 37 -90");
        employee.setTelephone("3004819089");
        employee.setCreateAt(Calendar.getInstance().getTime());
        employee.setMail("prueba@gmail.com");
        employee.setAge("19");
        return employee;
    }
}

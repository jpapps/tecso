package coop.tecso.hulk.api.enums;

public enum APIProductCodeEnum {

    PRD001("PRD001 - product created correctly"),
    PRD002("PRD002 - An error occurred while creating the product"),
    PRD003("PRD003 - Product listed correctly"),
    PRD004("PRD003 - product update correctly"),
    PRD005("PRD003 - An error occurred while updating the product"),
    PRD006("PRD003 - An error occurred while deleting the product"),
    PRD007("PRD003 - product deleted correctly"),
    PRD008("PRD003 - An error occurred while listening the product");

    private String description;

    APIProductCodeEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

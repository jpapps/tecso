package coop.tecso.hulk.api.enums;

public enum APITransactionCodeEnum {

    TRA001("TRA001 - transaction created correctly"),
    TRA002("TRA002 - An error occurred while creating the transaction"),
    TRA003("TRA003 - you must have at least one product to continue the transaction"),
    TRA004("TRA004 - must have an employee associated with the transaction"),
    TRA005("TRA005 - you must have a customer associated with the transaction"),
    TRA007("TRA007 - Transactions listed correctly"),
    TRA008("TRA008 - problem listing transactions");

    private String description;

    APITransactionCodeEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

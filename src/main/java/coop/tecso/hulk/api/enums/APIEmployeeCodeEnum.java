package coop.tecso.hulk.api.enums;

public enum APIEmployeeCodeEnum {

    EMP001("EMP001 - employees listed correctly"),
    EMP002("EMP002 - An error occurred when listing employees");

    private String description;

    APIEmployeeCodeEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

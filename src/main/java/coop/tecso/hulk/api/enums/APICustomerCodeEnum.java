package coop.tecso.hulk.api.enums;

public enum APICustomerCodeEnum {

    CUS001("TRA001 - customer created correctly"),
    CUS002("TRA002 - An error occurred while creating the customer"),
    CUS003("TRA003 - An error occurred while getting the clients"),
    CUS004("TRA004 - customer list successfully obtained");

    private String description;

    APICustomerCodeEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

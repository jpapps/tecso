package coop.tecso.hulk.api.enums;

public enum StatusTransactionEnum {

    IN_PROGRESS,
    FINISH,
    CANCELED;

}

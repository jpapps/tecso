package coop.tecso.hulk.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class TransactionProductDTO implements Serializable {

    private static final long serialVersionUID = 3298883053614722984L;

    private Long id;

    private ProductDTO productDTO;

    private TransactionDTO transactionDTO;

    private Integer units;
}

package coop.tecso.hulk.api.dto;

import coop.tecso.hulk.api.enums.StatusTransactionEnum;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class TransactionDTO implements Serializable {

    private static final long serialVersionUID = 7052711904915764028L;

    private Long id;

    private EmployeeDTO employeeDTO;

    private CustomerDTO customerDTO;

    private List<TransactionProductDTO> transactionProductDTOList;

    private StatusTransactionEnum status;

    private Date createdAt;

    private Date updatedAt;

}

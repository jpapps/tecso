package coop.tecso.hulk.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class CustomerDTO implements Serializable {

    private static final long serialVersionUID = 2187106974981974513L;

    private Long id;

    private String name;

    private String mail;

    private String age;

    private String address;

    private String telephone;

    private Date createAt;

    private Date updateAt;

    private Date deleteAt;
}

package coop.tecso.hulk.api.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Getter
@Setter
public class ResponseDTO<T> implements Serializable {

    private static final long serialVersionUID = -1098432130439863927L;

    private String message;

    private HttpStatus httpStatus;

    private Page<T> objectPage;

    private T object;

    private String exception;

}

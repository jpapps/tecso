package coop.tecso.hulk.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class ProductDTO implements Serializable {

    private static final long serialVersionUID = 2572855784713140924L;

    private Long id;

    private String code;

    private String description;

    private Integer quantity;

    private Double price;

    private Date createAt;

    private Date updateAt;

    private Date deleteAT;
}

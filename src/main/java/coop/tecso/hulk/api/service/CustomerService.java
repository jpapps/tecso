package coop.tecso.hulk.api.service;

import coop.tecso.hulk.api.dto.CustomerDTO;
import coop.tecso.hulk.api.dto.ResponseDTO;

public interface CustomerService {

    /**
     * save
     *
     * @param customerDTO
     * @return
     */
    ResponseDTO save(final CustomerDTO customerDTO);

    /**
     * listAll
     *
     * @param page
     * @param size
     * @return
     */
    ResponseDTO listAll(Integer page, Integer size);
}

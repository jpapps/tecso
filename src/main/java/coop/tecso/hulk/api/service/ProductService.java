package coop.tecso.hulk.api.service;

import coop.tecso.hulk.api.dto.ProductDTO;
import coop.tecso.hulk.api.dto.ResponseDTO;

public interface ProductService {


    ResponseDTO save(final ProductDTO productDTO);


    ResponseDTO update(final ProductDTO productDTO);


    ResponseDTO delete(final Long id);


    ResponseDTO getById(final Long id);


    ResponseDTO listAll(Integer page, Integer size);
}

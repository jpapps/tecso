package coop.tecso.hulk.api.service;

import coop.tecso.hulk.api.dto.ResponseDTO;
import coop.tecso.hulk.api.dto.TransactionDTO;

public interface TransactionService {

    /**
     * save
     *
     * @param transactionDTO
     * @return
     */
    ResponseDTO save(final TransactionDTO transactionDTO);

    /**
     * listAll
     *
     * @param page
     * @param size
     * @return
     */
    ResponseDTO listAll(Integer page, Integer size);
}

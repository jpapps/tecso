package coop.tecso.hulk.api.service;

import coop.tecso.hulk.api.dto.ResponseDTO;

public interface EmployeeService {

    /**
     * getEmployes
     *
     * @param page
     * @param size
     * @return
     */
    ResponseDTO getEmployes(Integer page, Integer size);

}

package coop.tecso.hulk.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "EMPLOYEE")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "MAIL", unique = true, nullable = false)
    private String mail;

    @Column(name = "AGE", nullable = false)
    private String age;

    @Column(name = "ADDRESS", nullable = false)
    private String address;

    @Column(name = "TELEPHONE", nullable = false)
    private String telephone;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_AT", nullable = false)
    private Date createAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATE_AT")
    private Date updateAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DELETE_AT")
    private Date deleteAt;


}
